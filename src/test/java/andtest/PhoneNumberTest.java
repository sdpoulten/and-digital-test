package andtest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@AutoConfigureMockMvc
public class PhoneNumberTest {

    private static final String URL_TEMPLATE_ACTIVATE_PHONE_NUMBER = "/activatePhoneNumber?phoneNumber={number}&customerId={customerId}";
    private static final String URL_TEMPLATE_GET_ALL_PHONE_NUMBERS = "/phoneNumbers";
    private static final String URL_TEMPLATE_GET_PHONE_NUMBERS_FOR_ID = "/phoneNumbers?customerId={customerId}";

    @Autowired
    private MockMvc mockMvc;

    @Before
    public void clearAllPhoneNumbers()
    {
        PhoneNumberController.clearPhoneNumbers();
    }

    @Test
    public void activatePhoneNumberReturnsCreatedPhoneNumber() throws Exception {
        this.mockMvc.perform(post(URL_TEMPLATE_ACTIVATE_PHONE_NUMBER, "01222123456", "2"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json("{'customerId':2,'phoneNumber':'01222123456'}"));
        //TODO - Need to check ID?
    }

    @Test
    public void activatePhoneNumberWithInvalidCustomerIdReturns404() throws Exception
    {
        long invalidCustomerId = PhoneNumberController.MAX_CUSTOMER_ID_PLACEHOLDER + 1;
        this.mockMvc.perform(post(URL_TEMPLATE_ACTIVATE_PHONE_NUMBER, "01222123458", invalidCustomerId))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void getPhoneNumbersReturnsPhoneNumberWeAdded() throws Exception
    {
        this.mockMvc.perform(post(URL_TEMPLATE_ACTIVATE_PHONE_NUMBER, "01222123456", "2"));

        this.mockMvc.perform(get(URL_TEMPLATE_GET_ALL_PHONE_NUMBERS))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json("[{'customerId':2,'phoneNumber':'01222123456'}]"));
    }

    @Test
    public void getPhoneNumbersForCustomerOnlyReturnsPhoneNumbersForCustomer() throws Exception
    {
        //Activate four phone numbers, for three different customers
        this.mockMvc.perform(post(URL_TEMPLATE_ACTIVATE_PHONE_NUMBER, "01222123456", "1"));
        this.mockMvc.perform(post(URL_TEMPLATE_ACTIVATE_PHONE_NUMBER, "01222123457", "2"));
        this.mockMvc.perform(post(URL_TEMPLATE_ACTIVATE_PHONE_NUMBER, "01222123458", "2"));
        this.mockMvc.perform(post(URL_TEMPLATE_ACTIVATE_PHONE_NUMBER, "01222123459", "3"));

        //Test getting multiple phone numbers for a single customer
        this.mockMvc.perform(get(URL_TEMPLATE_GET_PHONE_NUMBERS_FOR_ID, 2))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(content().json("[{'customerId':2,'phoneNumber':'01222123457'},{'customerId':2,'phoneNumber':'01222123458'}]"));

        //Test getting a single phone numbers for a single customer
        this.mockMvc.perform(get(URL_TEMPLATE_GET_PHONE_NUMBERS_FOR_ID, 1))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json("[{'customerId':1,'phoneNumber':'01222123456'}]"));
    }

    @Test
    public void getPhoneNumbersWithInvalidCustomerIdReturns404() throws Exception
    {
        long invalidCustomerId = PhoneNumberController.MAX_CUSTOMER_ID_PLACEHOLDER + 1;
        this.mockMvc.perform(get(URL_TEMPLATE_GET_PHONE_NUMBERS_FOR_ID, invalidCustomerId))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void gettingNoPhoneNumbersForValidCustomerId() throws Exception
    {
        this.mockMvc.perform(get(URL_TEMPLATE_GET_PHONE_NUMBERS_FOR_ID, "1"))
                .andDo(print())
                .andExpect(status().isOk());
    }
}