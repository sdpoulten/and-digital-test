package andtest;

public class PhoneNumber {
    private final long id;
    private final long customerId;
    private final String phoneNumber;

    public PhoneNumber(long id, long customerId, String phoneNumber) {
        this.id = id;
        this.customerId = customerId;
        this.phoneNumber = phoneNumber;
    }

    public long getId() {
        return id;
    }

    public long getCustomerId() {
        return customerId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }
}
