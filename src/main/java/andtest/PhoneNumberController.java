package andtest;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@RestController
public final class PhoneNumberController {
    static final long MAX_CUSTOMER_ID_PLACEHOLDER = 5;

    private static final List<PhoneNumber> phoneNumbers = new ArrayList<>(); //Store our phone numbers - should replace with a database.

    private final AtomicLong counter = new AtomicLong(); //Keep track of ids.

    /**
     * Call to activate a phone number for a given customer ID - I'm assuming a separate customer table in our imaginary database.
     * Phone number accepts any non-empty string.
     * @param customerId should be a valid customer id in the database, if not found return a 404 error.
     * @param phoneNumber should be a non-empty string - if not supplied, will return a 400 error.
     * @return the phone number object that was created, including the database id.
     */
    @PostMapping("/activatePhoneNumber")
    public PhoneNumber activatePhoneNumber(@RequestParam(value="customerId", defaultValue="-1") long customerId, @RequestParam(value="phoneNumber", defaultValue="") String phoneNumber)
    {
        if (!customerIdExists(customerId))
        {
            throw new CustomerNotFoundException();
        }

        PhoneNumber newPhoneNumber = new PhoneNumber(counter.incrementAndGet(), customerId, phoneNumber);
        phoneNumbers.add(newPhoneNumber);
        return newPhoneNumber;
    }

    /**
     * API Call to get phone numbers
     * @param customerId optional - restricts search to a given customer ID - this should be a valid customer ID in the database, else we return a 404 error. If not provided, returns all phone numbers.
     * @return all phone numbers if a customer id is not provided, otherwise gets all the phone numbers for a particular customer.
     */
    @RequestMapping("/phoneNumbers")
    public List<PhoneNumber> getPhoneNumbers(@RequestParam(value="customerId", defaultValue="-1") long customerId)
    {
        if (!customerIdExists(customerId))
        {
            throw new CustomerNotFoundException();
        }

        return phoneNumbers.stream()
                .filter(phoneNumber -> phoneNumber.getCustomerId() == customerId || customerId == -1)
                .collect(Collectors.toList());
    }

    /**
     * Checks if a given customer id exists in the database. A test implementation to demonstrate the sample phone numbers API
     * @param customerId the customer id to check
     * @return true if the customer id exists, false otherwise.
     */
    private boolean customerIdExists(long customerId)
    {
        return customerId < MAX_CUSTOMER_ID_PLACEHOLDER;
    }

    /**
     * Exception to throw when you try and add a phone number or find phone numbers for a customer that doesn't exist.
     */
    @ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Customer not found")
    private class CustomerNotFoundException extends RuntimeException
    {
    }

    /**
     * Clears all phone numbers, needed for testing.
     */
    static void clearPhoneNumbers()
    {
        phoneNumbers.clear();
    }

}
